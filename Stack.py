#!/usr/bin/env python

class StackMaxLimitExceededError(Exception):
	pass

class StackEmptyError(Exception):
	pass

class StackUnwritableError(Exception):
	pass

class Stack(object):
	def __init__(self, with_stack=None, stack_max=None):
		self.stack = []
		self.with_stack = with_stack
		self.stack_max = stack_max

	def __str__(self):
		"""(self) -> String
		Returns the stack contents as a string """
		return str(self.stack)

	def count(self):
		"""(self) -> Int
		Returns the count of she stack """
		return len(self.stack)

	def push(self, obj):
		"""(self, obj) -> Void
		Tries to push an object onto the stack. Fails if the stack is already full """
		if self.stack_max and len(self.stack) == self.stack_max: raise StackMaxLimitExceededError
		else:
			self.stack.append(obj)

	def pop(self):
		"""(self) -> Void
		Removes the last object from the stack. Fails if the stack is already empty """
		if len(self.stack) == 0: raise StackEmptyError
		else:
			self.stack.pop()

	def is_empty(self):
		"""(self) -> Bool
		Returns a boolean that signifies whether the stack is empty or not """
		return self.stack == 0

	def mute(self):
		"""(self) -> Tuple
		Returns an immutable tuple that consists of the stacks objects """
		return tuple(self.stack)

	def enum(self):
		"""(self) -> Dictionary
		Returns a enumeration dictionary that consists of the stacks objects """
		return enumerate(self.stack)

	def find(self, obj):
		"""(self, obj) -> Bool
		Returns a boolean that signifies whether the object can be found in the stack or not """
		return obj in self.stack

	def find_index(self, obj):
		"""(self, obj) -> Object?
		 Tries to locate the index of an object in the stack. If it can't be found, returns None """
		for key,value in enumerate(self.stack):
			if self.stack[key] == obj:
				return key
		return None

	def range(self, start=0, stop=-1, step=1):
		"""(self, start, stop, step) -> Stack
		 Returns a new Stack object with slices from the specified rang e"""
		return Stack(with_stack=self.stack[start:stop:step])

	def write(self, path="desktop/", fname="stack", ftype="txt"):
		"""(self, path, fname, ftype) -> Exception?
		Attempts to write the stack to a specified file. Returns an error that will be None if the
		write was a success, or an Exception if an error occurred during writing """
		f = path + fname + "." + ftype
		error = None
		w = open(f, "wb")
		try:
			for obj in self.stack:
				w.write("%s\n" % str(obj))
		except Exception as e:
			error = e
		finally:
			w.close()
			return error

	def json(self, pretty_format=True):
		#todo this needs to be fixed
		"""(self, pretty_format) -> String
		 Converts the stack to json format """
		if pretty_format:
			return '''
			{
				'Stack' : %s,
			} ''' % self.stack
		else:
			return str(self.stack)

	def clear(self):
		"""(self) -> Void
		Removes all objects from the stack """
		for obj in self.stack:
			self.stack.pop()

	def description(self):
		"""(self) -> Self
		Returns the stack's objects as a string description """
		return self.stack 

	def low(self):
		"""(self) -> Object
		Returns the object at index 0 in the stack """
		if len(self.stack) == 0: raise StackEmptyError
		else:
			self.stack.sort()
			return self.stack[0]

	def high(self):
		"""(self) -> Object
		Returns the object at the last index in the stack """
		if len(self.stack) == 0: raise StackEmptyError
		else:
			self.stack.sort()
			return self.stack[len(self.stack) - 1]